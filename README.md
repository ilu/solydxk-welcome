# solydxk-welcome
Welcome screen for SolydXK and install help for some popular software.

## Translations
You can help translating solydxk-welcome on [Transifex](https://www.transifex.com/abalfoort/solydxk-welcome)
